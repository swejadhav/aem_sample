package com.sample.stepdefs;

import com.sample.pageobjects.loginPage;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class login {
	
	loginPage login = new loginPage();
	
	@Given("^I have accessed the AEM login screen$")
	public void i_have_accessed_the_AEM_login_screen() throws Throwable {
		login.access_AEM("http://authoraem-qa.scholastic.com");

	}

	@When("^I enter my valid credentials and click on Sign In$")
	public void i_enter_my_valid_credentials_and_click_on_Sign_In() throws Throwable {
		
		login.signIn();

	}
	
	@When("^I enter my valid credentials \'(.*)\' and \'(.*)\' and click on Sign In$")
	public void i_enter_my_valid_credentials_pnutakki_and_password_and_click_on_Sign_In(String id, String pwd) throws Throwable {
		
		login.signIn(id, pwd);

	}

	@Then("^I should be logged in succesfully$")
	public void i_should_be_logged_in_succesfully() throws Throwable {
		
		login.verify_login_succesful();
		login.teardown();

	}

}
