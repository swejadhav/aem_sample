package com.sample.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import junit.framework.Assert;

public class loginPage {
	
	public static WebDriver driver;

	public static WebDriver getDriver() {
		return driver;
	}

	public static void setDriver(WebDriver driver) {
		loginPage.driver = driver;
	}
	

	public void teardown()
	{
		driver.quit();
	}
	
	public void access_AEM(String url)
	{
	    System.setProperty("webdriver.chrome.driver", "C:\\Users\\swejadhav\\Softwares\\Drivers\\chromedriver.exe");
			
	    driver = new ChromeDriver();	

	    driver.get(url);
	}
	
	public void signIn() throws InterruptedException
	{
		System.out.println(driver.getTitle());
		
		WebElement userName= driver.findElement(By.id("username"));
		WebElement password = driver.findElement(By.id("password"));
		
		userName.clear();
		userName.sendKeys("pnutakki");
		
		password.clear();
		password.sendKeys("password");

		WebElement signInButton = driver.findElement(By.id("submit-button"));
		signInButton.click();
		
		Thread.sleep(2000);

	}
	
	public void signIn(String Id, String Pwd) throws InterruptedException
	{
		System.out.println(driver.getTitle());
		
		WebElement userName= driver.findElement(By.id("username"));
		WebElement password = driver.findElement(By.id("password"));
		
		userName.clear();
		userName.sendKeys(Id);
		
		password.clear();
		password.sendKeys(Pwd);

		WebElement signInButton = driver.findElement(By.id("submit-button"));
		signInButton.click();
		
		Thread.sleep(2000);
	}

	//@SuppressWarnings("deprecation")
	public void verify_login_succesful()
	{
		String currentURL = driver.getCurrentUrl();
		Assert.assertTrue(currentURL.equalsIgnoreCase("http://authoraem-qa.scholastic.com/projects.html/content/projects"));
	}

}
