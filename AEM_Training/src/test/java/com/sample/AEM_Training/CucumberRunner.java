package com.sample.AEM_Training;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)

@CucumberOptions(features = "features", dryRun = false, monochrome = true,

		glue = { "com.sample.stepdefs" }, format = { "pretty", "html:target/automation/html",
				"json:target/automation/loginJson.json", "junit:target/automation/taget_junit/LoginXml.xml" })

public class CucumberRunner {

}
